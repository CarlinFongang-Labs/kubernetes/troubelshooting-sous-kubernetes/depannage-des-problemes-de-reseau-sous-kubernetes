# 8. Résolution des problèmes de réseau Kubernetes

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Intro

Nous allons parler de la résolution des problèmes de réseau dans Kubernetes. Bien qu'il y ait beaucoup de choses à dire sur ce sujet, nous allons nous concentrer sur quelques techniques simples que vous pouvez utiliser pour le moment. Nous allons d'abord nous intéresser aux composants kube-proxy et DNS de l'architecture réseau de Kubernetes. Ensuite, nous parlerons d'un outil appelé netshoot et nous ferons une petite démonstration pratique.

# Kube-proxy et DNS Kubernetes

Kube-proxy et le DNS Kubernetes sont des composants liés au réseau Kubernetes. En plus de vérifier des éléments comme votre plugin réseau dans Kubernetes ou même votre infrastructure réseau sous-jacente, vous voudrez peut-être examiner kube-proxy et le DNS Kubernetes si vous rencontrez des problèmes de communication entre les composants de votre cluster Kubernetes.

Dans un cluster kubeadm, le DNS et kube-proxy s'exécutent tous deux sous forme de pods dans le namespace kube-system. Vous pouvez vérifier ces pods kube-system pour voir s'il y a des problèmes avec le DNS ou kube-proxy de Kubernetes.

# Outil netshoot

Un excellent conseil pour le dépannage réseau est de faire fonctionner un conteneur dans le cluster que vous pouvez ensuite utiliser pour exécuter des commandes et tester la fonctionnalité du réseau. Parfois, pour résoudre des problèmes de réseau, il faut le faire du point de vue de quelque chose qui fonctionne réellement dans le cluster. Il existe une image de conteneur particulière qui est un excellent outil pour cela : il s'agit de `nicolaka/netshoot`. Cette image contient une variété d'outils d'exploration et de dépannage réseau, dont beaucoup vous seront probablement familiers si vous connaissez Linux. Il suffit de créer un conteneur utilisant cette image, puis d'utiliser la commande `kubectl exec` pour exécuter des commandes à l'intérieur du conteneur netshoot et explorer votre réseau Kubernetes.

# Démonstration pratique

Voyons ce que cela donne dans notre cluster. Nous sommes connectés à notre nœud de contrôle, et nous allons commencer par vérifier les pods dans le namespace kube-system.

```sh
kubectl get pods -n kube-system
```

>![Alt text](img/image.png)
*liste de pods présent dans le namespace kube-system*

Nous pouvons voir les pods kube-proxy ici. Pour obtenir plus d'informations sur kube-proxy, nous pouvons consulter les journaux de ces pods avec la commande suivante :

```sh
kubectl logs -n kube-system kube-proxy-6vktx
```

>![Alt text](img/image-1.png)
*Sortie de logs du pod kube-proxy*

On n'observe pas grand chose dans ces logs pour le moment.

De même, pour vérifier le DNS Kubernetes, nous cherchons les pods qui commencent par coredns. Voici comment obtenir leurs journaux :

```sh
kubectl logs -n kube-system <nom_du_pod_coredns>
```

>![Alt text](img/image-2.png)
*Logs du pod coredns*

Maintenant, regardons l'outil netshoot. Pour commencer, nous allons créer un pod de base et un service pour tester netshoot. Créez un fichier YAML nommé `nginx-netshoot.yml` avec le contenu suivant :

```bash
nano nginx-netshoot.yml
```


```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-netshoot
spec:
  containers:
  - name: nginx
    image: nginx
---
apiVersion: v1
kind: Service
metadata:
  name: svc-netshoot
spec:
  selector:
    app: nginx-netshoot
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```

Ensuite, créez les objets avec la commande :

```sh
kubectl apply -f nginx-netshoot.yml
```

>![Alt text](img/image-3.png)
*Pod et service ClusterIP crée*

Maintenant, nous allons créer le pod netshoot. Créez un fichier nommé `netshoot.yml` avec le contenu suivant :

```bash
nano netshoot.yml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: netshoot
spec:
  containers:
  - name: netshoot
    image: nicolaka/netshoot
    command: ["sleep", "infinity"]
```

Créez le pod netshoot avec :

```sh
kubectl apply -f netshoot.yml
```

>![Alt text](img/image-4.png)
*Pod netshoot crée et en cours d'exécution*

Une fois le pod netshoot en cours d'exécution, nous pouvons créer un shell interactif dans ce pod pour explorer le réseau Kubernetes. Utilisez la commande suivante :

```sh
kubectl exec --stdin --tty netshoot -- /bin/sh
```



À partir de là, vous pouvez utiliser divers outils de diagnostic réseau comme `curl`, `ping` ou `nslookup` pour tester la connectivité. Par exemple, pour tester notre service :

```sh
curl svc-netshoot
```

Ou utiliser `ping` et `nslookup` pour d'autres tests :

```sh
ping svc-netshoot
nslookup svc-netshoot
```

# Récapitulatif

Dans cette leçon, nous avons discuté de kube-proxy et du DNS Kubernetes et de leur rôle dans la résolution des problèmes de réseau dans votre cluster Kubernetes. Nous avons également parlé de l'utilisation de l'outil netshoot pour explorer votre réseau Kubernetes et avons effectué une démonstration pratique de ces concepts. C'est tout pour cette leçon. Nous vous verrons dans la prochaine leçon.


# Reférences

https://kubernetes.io/docs/tasks/administer-cluster/dns-debugging-resolution/

https://kubernetes.io/docs/tasks/debug/debug-application/debug-service/

https://github.com/nicolaka/netshoot